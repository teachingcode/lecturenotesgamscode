* Implementation of the two-stage shipping problem presented
* in the "Lecture Notes on Stochastic Programming" assuming
* - the demand is normally distributed with mean 100 and standard deviation 12
* - the purchase cost is uniformly distributed in [1.5,2.1] 
* - the overuspply cost is uniformly distributed in [1.2,1.4]
* - the dependence between the random variables is described by their covariances.
* We generate realizations by solving a Property Matching Problem.
* Author: Giovanni Pantuso




* *******************************************
* First we generate realizations by
* solving a Property Matching Problem
* *******************************************
SET S "the set of scenarios" /1*5/;
SET RV "The set of random variables" /qb,qo,d/;
alias(RV,UV);

PARAMETERS
m(RV) "The mean of the three random variables"
      /qb 1.8
       qo 1.3
       d  100/

v(RV) "The variance of the three random variables"
      /qb 0.03
       qo 0.0033
       d  144/



TABLE cov(RV,RV) "The covariance matrix"
	    	 qb       qo  	    d
	  qb    0.03    0.00197    1.4532
	  qo    0.00197 0.0033     0.2052
	  d     1.4532  0.2052 	   144;
	  

VARIABLES
dist	"The total distance between the target and the empirical value of the properties"
r(RV,S) "The realization of random variable RV under scenario S"
em(RV)	"The empirical mean of RV"
ev(RV)	"The empirical variance of RV"
ecov(RV,RV) "The empirical covariance between two random variables"
pi(S)	"The probability of scenario S"
;

POSITIVE VARIABLES
pi(S)
;

EQUATIONS
distance "The expression of total distance between the target and the empirical value of the properties"
eqmean(RV) "The expression of the mean"
eqvar(RV) "The expression of the variance"
eqcov(RV,RV) "The expression of the covariances"
constrProbabilities "The constraint on the probabilities"
;

distance..  dist =e= sum(RV, power(em(RV) - m(RV),2)) + sum(RV, power(sqrt(ev(RV)) - sqrt(v(RV)),2))
+ sum((RV,UV), power(ecov(RV,UV) - cov(RV,UV),2));

eqmean(RV).. em(RV) =e= sum(S,pi(S)*r(RV,S));
eqvar(RV).. ev(RV) =e= sum(S,pi(S)*power((r(RV,S)-em(RV)),2));
eqcov(RV,UV).. ecov(RV,UV) =e= sum(S,pi(S)*(r(RV,S)-em(RV))*(r(UV,S) - em(UV)));
constrProbabilities.. sum(S,pi(S)) =e= 1;


pi.l(S) = 1/card(S);
MODEL pmp /distance,eqmean,eqvar,eqcov,constrProbabilities/;

SOLVE pmp USING nlp MINIMIZING dist;

DISPLAY
r.l
em.l
ev.l
ecov.l
pi.l
;

* ******************************
* Now that we have the scenarios we can 
* formulate and solve the stochastic
* shipment problem
* *******************************


PARAMETERS
d(S) "The demand for each scenario"
qB(S) "The purchase cost in the local market for each scenario"
qO(S) "The oversupply cost  for each scenario"
p(S) "The probability for each scenario"
;

* We associate the realizations we have generated to the parameters of the shipping problem
* By using .l we can retrieve the value of a decision variable after the optimization problem has been solved.
p(S) = pi.l(S);
d(S) = r.l('d',S);
qB(S) = r.l('qb',S);
qO(S) = r.l('qo',S);


SCALAR unitsAvailable "the number of units initially available" /100/;

VARIABLES

z	"total cost"
xSH	"amount shipped"
xST	"amount stored"
yB	"amount bought"
yO	"oversupply"
;


POSITIVE VARIABLES

xSH
xST
yB(S)
yO(S)
;


EQUATIONS

totalExpectedCost	"The expression of the objective function"
availability		"The amound shipped or stored"
demandSatisfaction(S)	"The satisfaction of demnd for each scenario"
;


totalExpectedCost..	 z =e= xSH + sum(S, p(S) * qB(S) * yB(S)) + sum(S, p(S) * qO(S) * yO(S)) ;

availability.. 	    	 xSH + xST =e= unitsAvailable;
demandSatisfaction(S)..	 yB(S) - yO(S) + xSH =e= d(S);


MODEL shipping /totalExpectedCost,availability,demandSatisfaction/;

SOLVE shipping USING lp MINIMIZING z;

DISPLAY

xSH.l
xST.l
yB.l
yO.l
d
qB
qO
p
;