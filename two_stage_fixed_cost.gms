* Implementation of the two-stage shipping problem presented
* in the "Lecture Notes on Stochastic Programming"
* by Giovanni Pantuso

SET S "the set of scenarios" /1,2,3/;

SCALAR unitsAvailable "the number of units initially available" /100/;

PARAMETERS
d(S) "The demand for each scenario"
     /1  85
      2  105
      3  120/


qB(S) "The purchase cost in the local market for each scenario"
      /1  1.7
       2  1.7
       3  1.7/

qO(S) "The oversupply cost  for each scenario"
      /1  1.2
       2  1.2
       3  1.2/

p(S) "The probability for each scenario"
      /1  0.3
       2  0.3
       3  0.4/
;


VARIABLES

z	"total cost"
xSH	"amount shipped"
xST	"amount stored"
yB	"amount bought"
yO	"oversupply"
;


POSITIVE VARIABLES

xSH
xST
yB(S)
yO(S)
;


EQUATIONS

totalExpectedCost	"The expression of the objective function"
availability		"The amound shipped or stored"
demandSatisfaction(S)	"The satisfaction of demnd for each scenario"
;


totalExpectedCost..	 z =e= xSH + sum(S, p(S) * qB(S) * yB(S)) + sum(S, p(S) * qO(S) * yO(S)) ;

availability.. 	    	 xSH + xST =e= unitsAvailable;
demandSatisfaction(S)..	 yB(S) - yO(S) + xSH =e= d(S);


MODEL shipping /all/;

SOLVE shipping USING lp MINIMIZING z;

DISPLAY
z.l
xSH.l
xST.l
yB.l
yO.l
;