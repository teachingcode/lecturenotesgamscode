* Implementation of the multistage shipping problem presented
* in the "Lecture Notes on Stochastic Programming" using
* a Node formulation. Author: Giovanni Pantuso



SETS
N			"The set of nodes" /n1*n7/
T 		     	"The set of stages" /t1*t3/
TofN(N,T)	     	"The stage corresponding to a node"
parent(N,N)		"The parent of a node"
;

alias (N,M);
* Creates the matching node-stage
* Node n1 is the root, and belongs to t1

TofN('n1','t1') = yes ;

* All nodes with an order between 2 and 3 belong to stage 2
TofN(N, 't2') = 1 < ord(n) and ord(n) < 4;
* All nodes with an order greater than 3 belong to stage 3
TofN(N, 't3') = ord(n) >= 4;

* Creates the parent nodes
* All nodes at stage 2 have the root as parent node
parent(N, 'n1')$TofN(N,'t2')   = yes;
* The nodes at stage 3 with order between 4 and 5 have n2 as parent
parent(N, 'n2')$(TofN(N,'t3')$(ord(N) >= 4  and ord(N) <= 5))     = yes;
* The nodes at stage 3 with order between 6 and 7 have n3 as parent
parent(n, 'n3')$(TofN(N,'t3')$(ord(N) >=6  and ord(N) <=7))    = yes;


PARAMETERS

prods(N) "The production at each node"
     /n1	  100
      n2	  70
      n3	  70
      n4	  0
      n5	  0
      n6	  0
      n7	  0/

d(N) "The demand for each node"
     /n1	  0
      n2	  85
      n3	  115
      n4	  93.5
      n5	  76.5
      n6	  126.5
      n7	  103.5/

qB(N) "The purchase cost in the local market for each node"
     /n1	  0
      n2	  1.5
      n3	  1.8
      n4	  1.65
      n5	  1.35
      n6	  1.98
      n7	  1.62/

qO(N) "The oversupply cost  for each node"
     /n1	  0
      n2	  1.2
      n3	  1.3
      n4	  1.32
      n5	  1.08
      n6	  1.43
      n7	  1.17/

p(N) "The probability for each node"
     /n1	  1.0
      n2	  0.4
      n3	  0.6
      n4	  0.16
      n5	  0.24
      n6	  0.24
      n7	  0.36/
;


VARIABLES

z	"total cost"
xSH(N)	"amount shipped"
xST(N)	"amount stored"
yB(N)	"amount bought"
yO(N)	"oversupply"
;


POSITIVE VARIABLES

xSH(N)
xST(N)
yB(N)
yO(N)
;


EQUATIONS

totalExpectedCost	"The expression of the objective function"
availability(N)		"The amound shipped or stored"
demandSatisfaction(N)	"The satisfaction of demnd for each scenario"
;


totalExpectedCost..	 z =e= sum(N$(TofN(N,'t1') or TofN(N,'t2')) , p(N)*xSH(N))
+ sum(N$(TofN(N,'t2') or TofN(N,'t3')), p(N) * (qB(N) * yB(N)  + qO(N) * yO(N))) ;

availability(N)$(TofN(N,'t1') or  TofN(N,'t2')).. 	 xSH(N) + xST(N)  =e= prods(N) + sum(M$(parent(N,M)),xST(M))$(TofN(N,'t2'));
demandSatisfaction(N)$(TofN(N,'t2') or  TofN(N,'t3'))..	 yB(N) - yO(N) + sum(M$(parent(N,M)),xSH(M)) =e= d(N) - sum(M$(parent(N,M)),yO(M))$(TofN(N,'t3'));

OPTION limrow = 100;
MODEL shipping /all/;

SOLVE shipping USING lp MINIMIZING z;

DISPLAY
xSH.l
xST.l
yB.l
yO.l
z.l
;