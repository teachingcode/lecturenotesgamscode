* Calculation of the VSS on the two-stage shipping problem presented
* in the "Lecture Notes on Stochastic Programming"
* by Giovanni Pantuso

SET S "the set of scenarios" /1,2,3/;

SCALAR unitsAvailable "the number of units initially available" /100/;

PARAMETERS
d(S) "The demand for each scenario"
     /1  85
      2  105
      3  120/


qB(S) "The purchase cost in the local market for each scenario"
      /1  1.5
       2  1.7
       3  2.0/

qO(S) "The oversupply cost  for each scenario"
      /1  1.2
       2  1.2
       3  1.2/

p(S) "The probability for each scenario"
      /1  0.3
       2  0.3
       3  0.4/

;


**************************************************
* We start by formulating and solving each scenario problem
**************************************************
VARIABLES

z	"total cost"
xSH	"amount shipped"
yB	"amount bought"
yO	"oversupply"
;


POSITIVE VARIABLES

xSH
yB
yO
;


EQUATIONS

totalCost	"The expression of the objective function"
availability		"The amound shipped or stored"
demandSatisfaction	"The satisfaction of demnd for each scenario"
;


totalCost..	 z =e= xSH + (qB("3") * yB) + (qO("3") * yO) ;
availability.. 	    	 xSH  =l= unitsAvailable;
demandSatisfaction..	 yB - yO + xSH =e= d("3");


MODEL mvp /totalCost,availability,demandSatisfaction/;

SOLVE mvp USING lp MINIMIZING z;

DISPLAY
z.l
XSH.l
yB.l
yO.l
;

