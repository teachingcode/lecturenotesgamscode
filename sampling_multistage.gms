* Implementation of the multistage shipping problem presented
* in the "Lecture Notes on Stochastic Programming" assuming
* - the demand at stage 2 is normally distributed with mean 100 and standard deviation 12
* - the demand at stage 3 is normally distributed with mean 98 and standard deviation 16
* - the purchase cost at stage 2 and 3 is uniformly distributed in [1.5,2.1] 
* - the overuspply cost at stage 2 and 3 is uniformly distributed in [1.2,1.4]
* - the random variables are independent.
* We generate realizations by sampling.
* Author: Giovanni Pantuso




* *******************************************
* First we generate realizations by
* solving a Property Matching Problem
* *******************************************
SETS
N "the set of nodes" /n1*n31/
RV "The set of random variables" /qb,qo,d/
T  "The set of stages" /t1*t3/
TofN(N,T) "the stage corresponding to a node"
parent(N,N) "The parent of a node"
;

alias(RV,UV);
alias(N,M);

* Creates the matching node-stage
* Node n1 is the root, and belongs to t1

TofN('n1','t1') = yes ;
* All nodes with an order between 2 and 6 belong to stage 2
TofN(N, 't2') = 2 <= ord(n) and ord(n) <= 6;
* All nodes with an order greater than 6 belong to stage 3
TofN(N, 't3') = ord(n) >= 7;

* Creates the parent nodes
* All nodes at stage 2 have the root as parent node
parent(N, 'n1')$TofN(N,'t2')   = yes;
* The nodes at stage 3 with order between 7 and 11 have n2 as parent, and so on
parent(N, 'n2')$(ord(N) >= 7  and ord(N) <=11)    = yes;
parent(n, 'n3')$(ord(N) >= 12 and ord(N) <=16)    = yes;
parent(n, 'n4')$(ord(N) >= 17 and ord(N) <=21)    = yes;
parent(n, 'n5')$(ord(N) >= 22 and ord(N) <=26)    = yes;
parent(n, 'n6')$(ord(N) >= 27 and ord(N) <=31)    = yes;



PARAMETERS

prods(N) "The production at each node"
     /n1	  100
      n2	  70
      n3	  70
      n4	  0
      n5	  0
      n6	  0
      n7	  0/
d(N) "The demand for each node"
qB(N) "The purchase cost in the local market for each node"
qO(N) "The oversupply cost  for each node"
p(N) "The probability for each node" 
;

* **********************************************
* We populate the parameters of the
* multistage shipment problem using
* sampled realizations
* **********************************************

d(N)$(ord(N) > 1 and ord(N) <=6) = normal(100,12);
d(N)$(ord(N) >= 7) = normal(98,16);
qB(N)$(ord(N) > 1) = uniform(1.5,2.1);
qO(N)$(ord(N) > 1) = uniform(1.2,1.4);
p('n1') = 1.0;
p(N)$(ord(N) >=2 and ord(N) <= 6) = 1/5;
p(N)$(ord(N) >=7) = 1/25;



VARIABLES

z	"total cost"
xSH(N)	"amount shipped"
xST(N)	"amount stored"
yB(N)	"amount bought"
yO(N)	"oversupply"
;


POSITIVE VARIABLES

xSH(N)
xST(N)
yB(N)
yO(N)
;


EQUATIONS

totalExpectedCost	"The expression of the objective function"
availability(N)		"The amound shipped or stored"
demandSatisfaction(N)	"The satisfaction of demnd for each scenario"
;


totalExpectedCost..	 z =e= sum(N$(TofN(N,'t1') or TofN(N,'t2')) , p(N)*xSH(N))
+ sum(N$(TofN(N,'t2') or TofN(N,'t3')), p(N) * (qB(N) * yB(N)  + qO(N) * yO(N))) ;

availability(N)$(TofN(N,'t1') or  TofN(N,'t2')).. 	 xSH(N) + xST(N)  =e= prods(N) + sum(M$(parent(N,M)),xST(M))$(TofN(N,'t2'));
demandSatisfaction(N)$(TofN(N,'t2') or  TofN(N,'t3'))..	 yB(N) - yO(N) + sum(M$(parent(N,M)),xSH(M)) =e= d(N) - sum(M$(parent(N,M)),yO(M))$(TofN(N,'t3'));

OPTION limrow = 100;
MODEL shipping /totalExpectedCost,availability,demandSatisfaction/;

SOLVE shipping USING lp MINIMIZING z;

DISPLAY
d
qB
qO
p
xSH.l
xST.l
yB.l
yO.l
;