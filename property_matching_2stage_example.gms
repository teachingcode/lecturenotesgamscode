


* *******************************************
* We generate realizations by
* solving a Property Matching Problem
* Three RVs: xiA, xiB, xiC
* We want to match means, variances and covariances
* We want 5 realizations
* *******************************************

SET S "the set of scenarios" /1*5/;

Set RV "The set of random variables" /xiA,xiB,xiC/;
alias(RV,UV);

PARAMETERS
m(RV) "The mean of the three random variables"
      /xiA 180
       xiB 130
       xiC 200/

v(RV) "The variance of the three random variables"
      /xiA 300
       xiB 500
       xiC 900/



TABLE cov(RV,RV) "The covariance matrix"
	    	 xiA       xiB  	    xiC
	  xiA    300.00    39.00    	    458.00
	  xiB    39.00 	   500.00     	    335.00
	  xiC    458.00    335.00 	    900.00;
	  

VARIABLES
dist	"The total distance between the target and the empirical value of the properties"
r(RV,S) "The realization of random variable RV under scenario S"
em(RV)	"The empirical mean of RV"
ev(RV)	"The empirical variance of RV"
ecov(RV,RV) "The empirical covariance between two random variables"
pi(S)	"The probability of scenario S"
;

POSITIVE VARIABLES
pi(S)
;

EQUATIONS
distance "The expression of total distance between the target and the empirical value of the properties"
eqmean(RV) "The expression of the mean"
eqvar(RV) "The expression of the variance"
eqcov(RV,RV) "The expression of the covariances"
constrProbabilities "The constraint on the probabilities"
;

distance..  dist =e=  sum(RV, power(em(RV) - m(RV),2)) +  sum(RV, power(sqrt(ev(RV)) - sqrt(v(RV)),2))
+ sum((RV,UV), power(ecov(RV,UV) - cov(RV,UV),2));

eqmean(RV).. em(RV) =e= sum(S,pi(S)*r(RV,S));
eqvar(RV).. ev(RV) =e= sum(S,pi(S)*power((r(RV,S)-em(RV)),2));
eqcov(RV,UV).. ecov(RV,UV) =e= sum(S,pi(S)*(r(RV,S)-em(RV))*(r(UV,S) - em(UV)));
constrProbabilities.. sum(S,pi(S)) =e= 1;


pi.l(S) = 1/ord(S);
MODEL pmp /distance,eqmean,eqvar,eqcov,constrProbabilities/;

SOLVE pmp USING nlp MINIMIZING dist;

DISPLAY
r.l
pi.l
em.l
ev.l
ecov.l
dist.l
;

