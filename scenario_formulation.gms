* Implementation of the multistage shipping problem presented
* in the "Lecture Notes on Stochastic Programming" using
* a scenario formulation. Author: Giovanni Pantuso



SETS
S			"The set of scenarios" /s1*s4/
T 		     	"The set of stages" /t1*t3/
share(S,S,T)		"A dynamic set which says whether two scenarios share the same history at a certain stage"
;

alias (S,Z);

* All scenarios share the same history at stage 1
share('s1',Z,'t1') = yes;
* The rest of the tree
share('s1','s2','t2') = yes;
share('s3','s4','t2') = yes;

PARAMETERS

prods(T) "The production at each node"
     /t1	  100
      t2	  70
      t3	  0/
;

TABLE d(T,S) "The demand for each scenario"
      	  s1      s2	 s3  	 s4
     t1	  0	  0	 0	 0
     t2	  85	  85	 115	 115
     t3	  93.5	  76.5	 126.5	 103.5
     ;
     
TABLE qB(T,S) "The purchase cost in the local market for each scenario"
 	  s1      s2	 s3  	 s4
     t1	  0	  0	 0	 0
     t2	  1.5	  1.5	 1.8	 1.8
     t3	  1.65	  1.35	 1.98	 1.62
     ;
     
TABLE qO(T,S) "The oversupply cost for each scenario"
	  s1      s2	 s3  	 s4
     t1	  0	  0	 0	 0
     t2	  1.2	  1.2	 1.3	 1.3
     t3	  1.32	  1.08	 1.43	 1.17
     ;
 

PARAMETERS p(S) "The probability for each node"
     /s1	  0.16
      s2	  0.24
      s3	  0.36
      s4	  0.24/
;


VARIABLES

ec	"total expected cost"
xSH(T,S)	"amount shipped"
xST(T,S)	"amount stored"
yB(T,S)	"amount bought"
yO(T,S)	"oversupply"
;


POSITIVE VARIABLES

xSH(T,S)
xST(T,S)
yB(T,S)
yO(T,S)
;


EQUATIONS

totalExpectedCost	"The expression of the objective function"
availability(T,S)		"The amound shipped or stored"
demandSatisfaction(T,S)	"The satisfaction of demnd for each scenario"
nacsXSH(T,S,Z)		"The nonanticipativity constraints about xSH"
nacsXST(T,S,Z)		"The nonanticipativity constraints about xST"
nacsYB(T,S,Z)		"The nonanticipativity constraints about yB"
nacsYO(T,S,Z)		"The nonanticipativity constraints about yO"
;


totalExpectedCost..	 ec =e= sum((T,S)$(ord(T) < card(T)), p(S) * xSH(T,S) )
+ sum((T,S)$(ord(T) > 1), p(S) * qB(T,S) * yB(T,S))
+ sum((T,S)$(ord(T) > 1), p(S) * qO(T,S) * yO(T,S) );


availability(T,S)$(ord(T) < card(T)).. 	 xSH(T,S) + xST(T,S)  =e= prods(T) + xST(T-1,S)$(ord(T) > 1);

demandSatisfaction(T,S)$(ord(T) > 1)..	 yB(T,S) - yO(T,S) + xSH(T-1,S) =e= d(T,S) - yO(T-1,S)$(ord(T) > 2) ;

nacsXSH(T,S,Z)$(ord(T) < card(T) and share(S,Z,T))..			 xSH(T,S) - xSH(T,Z) =e= 0;
nacsXST(T,S,Z)$(ord(T) < card(T) and share(S,Z,T))..			 xST(T,S) - xST(T,Z) =e= 0;
nacsYB(T,S,Z)$(ord(T) > 1 and share(S,Z,T))..			 yB(T,S) - yB(T,Z) =e= 0;
nacsYO(T,S,Z)$(ord(T) > 1 and share(S,Z,T))..			 yO(T,S) - yO(T,Z) =e= 0;

OPTION limrow = 100;
MODEL shipping /all/;

SOLVE shipping USING lp MINIMIZING ec;

DISPLAY
xSH.l
xST.l
yB.l
yO.l
ec.l
;