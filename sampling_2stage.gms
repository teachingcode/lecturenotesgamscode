* Implementation of the two-stage shipping problem presented
* in the "Lecture Notes on Stochastic Programming" assuming
* - the demand is uniformly distributed in [80,120]
* - the purchase cost is uniformly distributed in [1.5,2.2] 
* - the overuspply cost is deterministic
* - the three random variables are independent
* by Giovanni Pantuso

* Resets the seed to current clock
execseed = 1+gmillisec(jnow);

SET S "the set of scenarios" /1*5/;

SCALAR unitsAvailable "the number of units initially available" /100/;

PARAMETERS
d(S) "The demand for each scenario"
qB(S) "The purchase cost in the local market for each scenario"
qO(S) "The oversupply cost  for each scenario"
p(S) "The probability for each scenario"
;

* When drawing iid samples the probability of each scenario is the same: 1/S
p(S) = 1/card(S);
* For each element in S draws an iid sample from U(80,120)
d(S) = uniform(80,120);
* For each element in S draws an iid sample from U(1.5,2.2)
qB(S) = uniform(1.5,2.2);
* qO is deterministic
qO(S) = 1.2;

VARIABLES

z	"total cost"
xSH	"amount shipped"
yB	"amount bought"
yO	"oversupply"
;


POSITIVE VARIABLES

xSH
yB(S)
yO(S)
;


EQUATIONS

totalExpectedCost	"The expression of the objective function"
availability		"The amound shipped or stored"
demandSatisfaction(S)	"The satisfaction of demnd for each scenario"
;


totalExpectedCost..	 z =e= xSH + sum(S, p(S) * qB(S) * yB(S)) + sum(S, p(S) * qO(S) * yO(S)) ;

availability.. 	    	 xSH  =l= unitsAvailable;
demandSatisfaction(S)..	 yB(S) - yO(S) + xSH =e= d(S);

* To calculate an upper bound fix xSH
xSH.fx = 82.9;


MODEL shipping /all/;

SOLVE shipping USING lp MINIMIZING z;

DISPLAY


yB.l
yO.l
d
qB
qO
p
z.l
xSH.l
;