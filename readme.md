# Lecture Notes on Stochastic Programming

This repository contains the GAMS source code for a number of examples discussed in **Lecture Notes on Stochastic Programming** by Giovanni Pantuso. Most of these examples are obtained by extending the model introduced by G.B. Dantzig in *Linear Programming Under Uncertainty*,Management Science,1,3-4,197--206,1955. 

## Files in this repository
* [Two-stage stochastic shipment problem (Vignette 3)](two_stage.gms) 
* [Node formulation of the multi-stage stochastic shipment problem (Vignette 6)](node_formulation.gms)
* [Scenario formulation of the multi-stage stochastic shipment problem (Vignette 7)](scenario_formulation.gms)
* [Two-stage stochastic shipment problem with sampled realizations (Vignette 8)](sampling_2stage.gms)
* [Two-stage stochastic shipment problem with property-matching realizations (Vignette 10)](property_matching_2stage.gms)
* [Multistage stochastic shipment problem with sampled realizations (Vignette 9)](sampling_multistage.gms)
* [Multistage stochastic shipment problem with property-matching realizations (Vignette 11)](property_matching_multistage.gms)


## Author, Purpose & Usage
The author of the files in this repository is Giovanni Pantuso, associate professor of Operations Research at the University of Copenhagen.
The purpose of these files is solely that of contributing, together with the **Lecture Notes on Stochastic Programming** to supporting the learning process of
the students of the course **Advanced Operations Research: Stochastic Programming** at the Department of Mathematical Sciences.
The files can be repoduced, modified and extended in any way provided that the original work is cited.