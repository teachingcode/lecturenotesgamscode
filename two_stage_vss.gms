* Calculation of the VSS on the two-stage shipping problem presented
* in the "Lecture Notes on Stochastic Programming"
* by Giovanni Pantuso

SET S "the set of scenarios" /1,2,3/;

SCALAR unitsAvailable "the number of units initially available" /100/;

PARAMETERS
d(S) "The demand for each scenario"
     /1  85
      2  105
      3  120/


qB(S) "The purchase cost in the local market for each scenario"
      /1  1.5
       2  1.7
       3  2.0/

qO(S) "The oversupply cost  for each scenario"
      /1  1.2
       2  1.2
       3  1.2/

p(S) "The probability for each scenario"
      /1  0.3
       2  0.3
       3  0.4/

mean_demand
mean_qO
mean_qB
;

mean_demand = sum(S, p(S) * d(S));
mean_qO = sum(S, p(S) * qO(S));
mean_qB = sum(S, p(S) * qB(S));


display
mean_demand, mean_qO, mean_qB;

**************************************************
* We start by formulating and solving the EVP
**************************************************
VARIABLES

mvp_z	"total cost"
mvp_xSH	"amount shipped"
mvp_yB	"amount bought"
mvp_yO	"oversupply"
;


POSITIVE VARIABLES

mvp_xSH
mvp_yB
mvp_yO
;


EQUATIONS

mvp_totalCost	"The expression of the objective function"
mvp_availability		"The amound shipped or stored"
mvp_demandSatisfaction	"The satisfaction of demnd for each scenario"
;


mvp_totalCost..	 mvp_z =e= mvp_xSH + (mean_qB * mvp_yB) + (mean_qO * mvp_yO) ;
mvp_availability.. 	    	 mvp_xSH  =l= unitsAvailable;
mvp_demandSatisfaction..	 mvp_yB - mvp_yO + mvp_xSH =e= mean_demand;


MODEL mvp /mvp_totalCost,mvp_availability,mvp_demandSatisfaction/;

SOLVE mvp USING lp MINIMIZING mvp_z;

DISPLAY
mvp_z.l
mvp_xSH.l
mvp_yB.l
mvp_yO.l
;


* We store the value of the first stage solution in the MVP
PARAMETER
MVPXSH
;

MVPXSH = mvp_xSH.l;
;

display
MVPXSH
;
********************************************
* Stochastic program
********************************************

VARIABLES

z	"total cost"
xSH	"amount shipped"
yB	"amount bought"
yO	"oversupply"
;


POSITIVE VARIABLES

xSH
yB(S)
yO(S)
;


EQUATIONS

totalExpectedCost	"The expression of the objective function"
availability		"The amound shipped or stored"
fix_firststageXSH       "We fix the first-stage variables to the solution of the MVP"
demandSatisfaction(S)	"The satisfaction of demnd for each scenario"
;


totalExpectedCost..	 z =e= xSH + sum(S, p(S) * qB(S) * yB(S)) + sum(S, p(S) * qO(S) * yO(S)) ;

availability.. 	    	 xSH  =e= unitsAvailable;
fix_firststageXSH..	 xSH =e= MVPXSH;
demandSatisfaction(S)..	 yB(S) - yO(S) + xSH =e= d(S);


MODEL shipping /totalExpectedCost,availability,fix_firststageXSH,demandSatisfaction/;

SOLVE shipping USING lp MINIMIZING z;

DISPLAY
z.l
xSH.l
yB.l
yO.l
;